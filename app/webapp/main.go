package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var requestCounter = prometheus.NewCounter(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "HTTP Request Counter",
	},
)

func main() {

	h := http.NewServeMux()

	prometheus.MustRegister(requestCounter)

	h.Handle("/metrics", promhttp.Handler())

	h.HandleFunc("/liveness", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "OK")
		log.Println("We are alive! :)")
	})

	h.HandleFunc("/readiness", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "OK")
		log.Println("We are healthy and ready for traffic! :)")
	})

	h.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		fmt.Fprintf(w, "Hello %s", os.Getenv("MEETUP_TEXT"))
		duration := time.Now().Sub(startTime)
		log.Println("Request took", duration)
		requestCounter.Inc()
	})

	log.Fatal(http.ListenAndServe(":8080", h))
}
